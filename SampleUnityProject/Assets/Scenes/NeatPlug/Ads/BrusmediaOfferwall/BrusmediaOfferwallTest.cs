/**
 * BrusmediaOfferwallTest.cs
 * 
 * A Test script for demonstrating the usage of BrusmediaOfferwall Plugin.
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-brusmedia-offerwall-plugin to find information 
 * about how to integrate and use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2014 NeatPlug.com All Rights Reserved. 
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BrusmediaOfferwallTest : MonoBehaviour {	
	
	private float _buttonWidth =  (Mathf.Min(Screen.height, Screen.width) - 40f) / 3f;	
	private float _buttonHeight = 60f;	
	
	void OnGUI() {	
		
		if (UnityEngine.GUI.Button (			
			new Rect(Mathf.Min(Screen.height, Screen.width) / 2f - _buttonWidth / 2f, 
			100f, 
			_buttonWidth, _buttonHeight), 
			"Open OfferWall")) {		
			BrusmediaOfferwall.Instance().Open();
		}
		
		if (UnityEngine.GUI.Button (new Rect(10f, 200f, _buttonWidth, _buttonHeight), "Start Timer")) {		
			BrusmediaOfferwall.Instance().StartTimer(30);
		}
		
		if (UnityEngine.GUI.Button (new Rect(20f + _buttonWidth, 200f, _buttonWidth, _buttonHeight), "Pause Timer")) {		
			BrusmediaOfferwall.Instance().PauseTimer();
		}		
		
		if (UnityEngine.GUI.Button (new Rect(30f + 2f * _buttonWidth, 200f, _buttonWidth, _buttonHeight), "Resume Timer")) {		
			BrusmediaOfferwall.Instance().ResumeTimer();
		}			
		
		if (UnityEngine.GUI.Button (new Rect(10f, 300f, _buttonWidth, _buttonHeight), "Restart Timer")) {			
			BrusmediaOfferwall.Instance().RestartTimer();
		}		
		
		if (UnityEngine.GUI.Button (new Rect(20f + _buttonWidth, 300f, _buttonWidth, _buttonHeight), "Stop Timer")) {		
			BrusmediaOfferwall.Instance().StopTimer();
		}		
		
	}		
	
	// Quit test app when back button is tapped.
	void Update () {
	
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{					
			Application.Quit();
		}	
	}
}
