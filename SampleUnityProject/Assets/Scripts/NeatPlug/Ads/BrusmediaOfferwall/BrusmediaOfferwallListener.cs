/**
 * BrusmediaOfferwallListener.cs
 * 
 * BrusmediaOfferwallListener listens to the Brusmedia OfferWall events.
 * File location: Assets/Scripts/NeatPlug/Ads/BrusmediaOfferwall/BrusmediaOfferwallListener.cs
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-brusmedia-offerwall-plugin to find information 
 * about how to integrate and use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2014 NeatPlug.com All Rights Reserved.
 * 
 * @version 1.0.0
 * @sdk_version(android) 1.0.0
 * @sdk_version(ios) 1.0.0
 *
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BrusmediaOfferwallListener : MonoBehaviour {	
	
	// Do not modify the values below.
	private bool _debug = false;	
	private static bool _instanceFound = false;
	
	void Awake()
	{
		// Do not modify the codes below.		
		BrusmediaOfferwallAgent.RetrieveDebugMode(ref _debug);
		BrusmediaOfferwallAgent.RetainGameObject(ref _instanceFound, gameObject, null);
		BrusmediaOfferwall.Instance();
	}
	
	void OnEnable()
	{
		// Register the Ad events.
		// Do not modify the codes below.	
		BrusmediaOfferwallAgent.OnOfferwallDismissed += OnOfferwallDismissed;
		BrusmediaOfferwallAgent.OnOfferwallCancelled += OnOfferwallCancelled;		
	}

	void OnDisable()
	{
		// Unregister the Ad events.
		// Do not modify the codes below.		
		BrusmediaOfferwallAgent.OnOfferwallDismissed -= OnOfferwallDismissed;
		BrusmediaOfferwallAgent.OnOfferwallCancelled -= OnOfferwallCancelled;	
	}	
	
	/**
	 * Fired when the OfferWall is dismissed.
	 * 
	 * @param resultDict
	 *           Dictionary<string, string> - A dictionary containing returned key-value pairs.
	 */
	void OnOfferwallDismissed(Dictionary<string, string> resultDict)
	{
		if (_debug)
		{
			Debug.Log (this.GetType().ToString() + " - OnOfferwallDismissed() Fired.");
			
			if (resultDict != null)
			{				
				List<string> keys = new List<string>(resultDict.Keys);				
				for (int i = 0; i < keys.Count; i++)
				{
					string key = keys[i];
					if (key != null)
					{
						string val = resultDict[key];
						if (val != null)
						{
							Debug.Log ("resultDict[\"" + key + "\"]: " + val);					           
						}
					}
				}				
			}
		}
		
		/// Your code here...
	}
	
	/**
	 * Fired when the OfferWall is cancelled by the user.
	 */
	void OnOfferwallCancelled()
	{
		if (_debug)
			Debug.Log (this.GetType().ToString() + " - OnOfferwallCancelled() Fired.");
		
		/// Your code here...
	}
	
}
