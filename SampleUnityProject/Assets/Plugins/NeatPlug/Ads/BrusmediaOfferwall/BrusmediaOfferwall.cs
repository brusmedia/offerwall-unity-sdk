/**
 * BrusmediaOfferwall.cs
 * 
 * A Singleton class encapsulating publicly accessible methods of Brusmedia OfferWall.
 * 
 * Please read the code comments carefully, or visit 
 * http://www.neatplug.com/integration-guide-unity3d-brusmedia-offerwall-plugin to find information how 
 * to use this program.
 * 
 * End User License Agreement: http://www.neatplug.com/eula
 * 
 * (c) Copyright 2014 NeatPlug.com All rights reserved.
 * 
 * @version 1.0.0
 * @sdk_version(android) 1.0.0
 * @sdk_version(ios) 1.0.0
 *
 */

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BrusmediaOfferwall  {
	
	#region Fields		
	
	private class BrusmediaOfferwallNativeHelper : IBrusmediaOfferwallNativeHelper {
		
#if UNITY_ANDROID	
		private AndroidJavaObject _plugin = null;
#endif		
		public BrusmediaOfferwallNativeHelper()
		{
			
		}
		
		public void CreateInstance(string className, string instanceMethod)
		{	
#if UNITY_ANDROID			
			AndroidJavaClass jClass = new AndroidJavaClass(className);
			_plugin = jClass.CallStatic<AndroidJavaObject>(instanceMethod);	
#endif			
		}
		
		public void Call(string methodName, params object[] args)
		{
#if UNITY_ANDROID			
			_plugin.Call(methodName, args);	
#endif
		}
		
		public void Call(string methodName, string signature, object arg)
		{
#if UNITY_ANDROID			
			var method = AndroidJNI.GetMethodID(_plugin.GetRawClass(), methodName, signature);			
			AndroidJNI.CallObjectMethod(_plugin.GetRawObject(), method, AndroidJNIHelper.CreateJNIArgArray(new object[] {arg}));
#endif			
		}
		
		public ReturnType Call<ReturnType> (string methodName, params object[] args)
		{
#if UNITY_ANDROID			
			return _plugin.Call<ReturnType>(methodName, args);
#else
			return default(ReturnType);			
#endif			
		}
	
	};	
	
	private static BrusmediaOfferwall _instance = null;
	
	#endregion
	
	#region Functions
	
	/**
	 * Constructor.
	 */
	private BrusmediaOfferwall()
	{	
#if UNITY_ANDROID		
		BrusmediaOfferwallAndroid.Instance().SetNativeHelper(new BrusmediaOfferwallNativeHelper());
#endif	
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance();
#endif
	}
	
	/**
	 * Instance method.
	 */
	public static BrusmediaOfferwall Instance()
	{		
		if (_instance == null) 
		{
			_instance = new BrusmediaOfferwall();
		}
		
		return _instance;
	}	
	
	/**
	 * Initialization.
	 * 
	 * @param apiKey
	 *            string - Your App API key.	
	 * 
	 * @param parameters
	 *            Dictionary<string, string> - Parameters in key-value pairs.	 
	 * 
	 * @param debugMode
	 *            bool - True for printing debug info in the log, false for not.
	 * 
	 */
	public void Init(string apiKey, Dictionary<string, string> parameters, bool debugMode)
	{
#if UNITY_ANDROID		
		BrusmediaOfferwallAndroid.Instance().Init(apiKey, parameters, debugMode);		
#endif	
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().Init(apiKey, parameters, debugMode);	
#endif		
	}
	
	/**
	 * Open the OfferWall.	 	
	 */
	public void Open()
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().Open();
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().Open();
#endif		
	}
	
	/**
	 * Start a timer to open the OfferWall peroidically.
	 * 
	 * @param intervalSeconds
	 *              int - The interval (in seconds) on how often to open the OfferWall.
	 *                    The value has to be greater than 0, or the default interval (60)
	 *                    will be used.
	 */
	public void StartTimer(int intervalSeconds)
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().StartTimer(intervalSeconds);
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().StartTimer(intervalSeconds);
#endif		
	}	
	
	/**
	 * Pause the started timer.	
	 * 
	 * This function preserves the duration already elapsed. 
	 * Call ResumeTimer() to start the timer again.
	 */
	public void PauseTimer()
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().PauseTimer();
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().PauseTimer();
#endif		
	}	
	
	/**
	 * Resume the paused timer.
	 * 
	 * This function is used in conjunction with PauseTimer() 
	 * to continue the timer from a previous elapsed duration.
	 */
	public void ResumeTimer()
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().ResumeTimer();
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().ResumeTimer();
#endif		
	}
	
	/**
	 * Restart the stopped timer.
	 *
	 * Used in conjunction with pause(). Restarts a timer using the orignial 
	 * interval, ignoring the previously elapsed duration before PauseTimer() 
	 * has been called.
	 */
	public void RestartTimer()
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().RestartTimer();
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().RestartTimer();
#endif		
	}	
	
	/**
	 * Stop the timer.
	 *
	 * IMPORTANT - It is the implementors responsibility to stop the timer 
	 * when it is no longer needed. This should be done when the timer is 
	 * no longer required. Once a timer has been stopped it can only be 
	 * restarted using StartTimer() as it is no longer considered initialised.
	 */
	public void StopTimer()
	{		
#if UNITY_ANDROID	
		BrusmediaOfferwallAndroid.Instance().StopTimer();
#endif
#if UNITY_IPHONE
		BrusmediaOfferwallIOS.Instance().StopTimer();
#endif		
	}	
	
	#endregion
}
