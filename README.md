# Brus Media OfferWall Unity SDK
Use this library to integrate the OfferWall into your Unity Projects

This SDK is implemented as a Unity Plugin.  The plugin wraps the features and functions in the corresponding Android & iOS SDKs.

This Plugin was written by NeatPlug.

## Setup
1. Locate the plugin that is relevant to your install inside the \Deliverables folder of this repositority.
2. Extract and following the instructions in the corresponding docs.html

## Using the OfferWall
See the sample project for a working example of the OfferWall including the timer feature.  You will need to provide a valid api key for the sample project.

